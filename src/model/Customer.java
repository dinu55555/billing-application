package model;

import org.json.simple.JSONObject;
import service.Validate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Customer extends Validate {

    static Map<String, String> discountMap = new HashMap<>();

    static {
        discountMap.put("10", "OFFER-10%");
        discountMap.put("20", "OFFER-20%");
        discountMap.put("30", "OFFER-30%");
        discountMap.put("40", "OFFER-40%");
        discountMap.put("50", "OFFER-50%");
    }


    public JSONObject jsonObject = new JSONObject();
    private String cusId = "CustomerId";
    private String userName = "UserName";
    private String password = "Password";
    private String role = "Role";
    private String currentStatus = "CurrentStatus";
    private String discount = "Discount";


    public Customer() throws IOException {
        jsonObject.put(userName, validateUserName());
        jsonObject.put(password, validatePassword());
        jsonObject.put(role, "customer");
        jsonObject.put(currentStatus,"Logged out");
        jsonObject.put(discount, showDiscount(discountMap));
    }

    public Customer(JSONObject jsonObject) {

        this.jsonObject = jsonObject;
    }


    public int getCusId() {

        return Integer.parseInt(jsonObject.get(cusId).toString());
    }

    public String getUserName() {

        return jsonObject.get(userName).toString();
    }

    public String getPassword() {

        return jsonObject.get(password).toString();
    }

    public String getRole() {

        return jsonObject.get(role).toString();
    }

    public void setStatus(String status) {

        jsonObject.put(currentStatus, status);
    }
    public String getDiscount()
    {
        return jsonObject.get(discount).toString();
    }

    public String getCurrentStatus() {

        return jsonObject.get(currentStatus).toString();
    }

    @Override
    public String toString() {
        return "Customer ID - " + this.getCusId() + "\nName - " + this.getUserName() + "\nPassword - " + this.getPassword() +
                "\nRole - " + this.getRole() + "\nStatus - " + this.getCurrentStatus();
    }


}
