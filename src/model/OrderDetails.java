package model;

import org.json.simple.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;


public class OrderDetails extends Items {

    private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    private Date date = new Date();

    private String orderedQuantity = "Ordered Quantity";
    private String orderedDate = "Ordered Date";
    private String totalPrice = "Total Order Price";
    private String discount = "Discount";


    public OrderDetails(JSONObject orderedObj, int quantity) {
        jsonObject.put(itemName, orderedObj.get(itemName).toString());
        jsonObject.put(category, orderedObj.get(category).toString());
        jsonObject.put(price, Double.parseDouble(orderedObj.get(price).toString()));
        jsonObject.put(orderedQuantity, quantity);
        jsonObject.put(itemId, Integer.parseInt(orderedObj.get(itemId).toString()));
        jsonObject.put(orderedDate, formatter.format(date));
        jsonObject.put(totalPrice, Double.parseDouble(Integer.toString(quantity * Integer.parseInt(orderedObj.get(price).toString()))));

    }

    /*public OrderDetails(JSONObject orderedObj) {

        jsonObject.put(itemName, orderedObj.get(itemName).toString());
        jsonObject.put(category, orderedObj.get(category).toString());
        jsonObject.put(price, Double.parseDouble(orderedObj.get(price).toString()));
        jsonObject.put(orderedQuantity, Integer.parseInt(orderedObj.get(orderedQuantity).toString()));
        jsonObject.put(itemId, Integer.parseInt(orderedObj.get(itemId).toString()));
        jsonObject.put(orderedDate, orderedObj.get(orderedDate));
        jsonObject.put(totalPrice, Double.parseDouble(orderedObj.get(totalPrice).toString()));


    }*/

    public OrderDetails(JSONObject orderedObj) {
        jsonObject=orderedObj;
    }

    public double getTotalPrice()
    {
        return Double.parseDouble(jsonObject.get(totalPrice).toString());
    }

    public int getOrderedQuantity() {

        return Integer.parseInt(jsonObject.get(orderedQuantity).toString());
    }

    public String getOrderedDate() {

        return jsonObject.get(orderedDate).toString();
    }
    public int getDiscount()
    {
        return Integer.parseInt(jsonObject.get(discount).toString());
    }

    @Override
    public String toString()
    {
        return "Item Id - " +this.getItemId()+"\nItem Name - "+this.getItemName()+"\nCategory " +
                "- "+this.getCategory()+"\nPrice Per Item - "+this.getPrice()+"\nOrdered Quantity - "
                +this.getOrderedQuantity()+"\nTotal amount - "+getTotalPrice()+"\nOrdered Date - "+this.getOrderedDate();
    }
}
