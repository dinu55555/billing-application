package model;

import org.json.simple.JSONObject;

public class Items {
    public JSONObject jsonObject = new JSONObject();

    String itemId = "Item id";
    String itemName = "Item Name";
    String category = "Item Category";
    String price = "Price Per Item";
    String availableQuantity = "Available Quantity";

    public Items() {
    }

    public Items(JSONObject object) {

        this.jsonObject = object;
    }

    public int getItemId() {

        return Integer.parseInt(jsonObject.get(itemId).toString());

    }

    public String getItemName() {

        return jsonObject.get(itemName).toString();
    }

    public void setIteamName(String newIteamName) {

        jsonObject.put(itemName, newIteamName);
    }

    public String getCategory() {

        return jsonObject.get(category).toString();
    }

    public void setCategory(String newCategory) {

        jsonObject.put(category, newCategory);
    }

    public double getPrice() {

        return Double.parseDouble(jsonObject.get(price).toString());
    }

    public void setPrice(double newPrice) {

        jsonObject.put(price, newPrice);
    }

    public int getAvailableQuantity() {

        return Integer.parseInt(jsonObject.get(availableQuantity).toString());
    }

    public void setAvailableQuantity(int newAvailableQuantity) {

        jsonObject.put(availableQuantity, newAvailableQuantity);
    }

    @Override
    public String toString()
    {
        return "Item Id - " +this.getItemId()+"\nItem Name - "+this.getItemName()+"\nCategory - "+this.getCategory()+"\nPrice Per Item - "+this.getPrice()+"\nAvailable Quantity - "+this.getAvailableQuantity();
    }
}