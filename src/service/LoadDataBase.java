package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class LoadDataBase {

    Connection getConnection(Connection c) {
        try {

            if (c == null) {

                Class.forName("org.postgresql.Driver");

                Connection con = DriverManager
                        .getConnection("jdbc:postgresql://localhost:5431/project",
                                "postgres", "5431");

                return con;
            }
            else
                return c;


        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return c;
    }
}

