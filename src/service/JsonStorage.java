package service;


import interfaces.Operations;
import model.Customer;
import model.Items;
import model.OrderDetails;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.*;

public class JsonStorage extends Sentences implements Operations {

    private static JSONObject customerJsonObj = new JSONObject();
    private static JSONObject itemJsonObj = new JSONObject();
    private static JSONObject orderJsonObj = new JSONObject();
    private static JSONObject orderIdMapping = new JSONObject();
    private static JSONObject purchaseJsonObj = new JSONObject();
    static int orderId;


    public JsonStorage() {
        customerJsonObj = loadFile("CustomerDetails.json");
        itemJsonObj = loadFile("ItemDetails.json");
        orderJsonObj = loadFile("OrderDetails.json");
        orderIdMapping = loadFile("OrderIdMapping.json");
        purchaseJsonObj = loadFile("PurchaseList.json");
        orderId = getOrderId();
    }


    public JSONObject placeOrder(JSONArray orders, int customerId, String discount) {

        JSONObject place = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        Set<String> custId = orderIdMapping.keySet();
        double totalAmount = 0, afterDiscount = 0;
        int count = 0;
        if (custId.contains(Integer.toString(customerId))) {
            jsonArray = (JSONArray) orderIdMapping.get(Integer.toString(customerId));
        }
        jsonArray.add(Integer.toString(orderId));
        for (Object obj : orders) {
            JSONObject temp = (JSONObject) obj;
            OrderDetails object = new OrderDetails(temp);
            place.put(Integer.toString(object.getItemId()), temp);
            totalAmount += object.getTotalPrice();
            int itemId = (Integer) temp.get("Item id");
            JSONObject temp1 = (JSONObject) itemJsonObj.get(Integer.toString(itemId));
            Set<String> items = (Set<String>) purchaseJsonObj.keySet();
            if (items.contains(itemId)) {
                count = Integer.parseInt(purchaseJsonObj.get(itemId).toString());
            }
            purchaseJsonObj.put(Integer.toString(itemId), object.getOrderedQuantity() + count);
            temp1.put(availableQuantity, Integer.parseInt(temp1.get(availableQuantity).toString()) - object.getOrderedQuantity());
            JSONObject discountRemove = (JSONObject) customerJsonObj.get(Integer.toString(customerId));
            JSONObject remove = (JSONObject) discountRemove.get("Discount");
            remove.remove(discount);


        }
        place.put("Amount Without Discount", totalAmount);
        place.put("Discount", (discount + " Precent"));
        afterDiscount = totalAmount - (Double.parseDouble(discount) / 100) * totalAmount;
        place.put("Final Amount with Discount", afterDiscount);
        orderJsonObj.put(Integer.toString(orderId++), place);
        orderIdMapping.put(Integer.toString(customerId), jsonArray);


        return place;
    }

    public JSONArray getOrderDetails(int customerId) {

        JSONArray getOrderDetails = new JSONArray();
        JSONArray orderId = (JSONArray) orderIdMapping.get(Integer.toString(customerId));
        System.out.println(orderId.toString());
        for (Object id : orderId) {
            String oId = (String) id;
            JSONObject temp = (JSONObject) orderJsonObj.get(oId);
            getOrderDetails.add(temp);
        }

        return getOrderDetails;
    }

    public String login(int id, String password) {
        String status = "", decryptPass;
        JSONObject obj;
        try {
            Set<String> cusIds = customerJsonObj.keySet();
            if (cusIds.contains(Integer.toString(id))) {
                obj = (JSONObject) customerJsonObj.get(Integer.toString(id));

                decryptPass = decrypt(obj.get(pass).toString());
                if (decryptPass.equals(password)) {
                    System.out.println("\t\tLogin Successful\n\t\t----------------");
                    System.out.println("\t\t Welcome " + obj.get("UserName"));
                    obj.put(currentStatus, active);

                    FileWriter write = new FileWriter("CustomerDetails.json");
                    write.write(customerJsonObj.toString());
                    write.close();

                    status = obj.get(role).toString().toLowerCase();
                } else
                    System.out.println("Password Incorrect");
            } else {
                System.out.println(invalidUserId);
                status = null;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return status;
    }

    public boolean logout(int id) {

        JSONObject obj = (JSONObject) customerJsonObj.get(Integer.toString(id));
        Customer cobject = new Customer(obj);
        cobject.setStatus("Logged out");
        obj = cobject.jsonObject;
        customerJsonObj.put(Integer.toString(id), obj);
        System.out.println("Logged out Successfully");
        saveToFile();
        return true;

    }

    public JSONObject updateItem(int id) {

        JSONObject obj = null;
        try {

            BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
            Set<String> itemIds = itemJsonObj.keySet();
            String updateValue, updateField;
            int updateIntValue;
            if (itemIds.contains(Integer.toString(id))) {
                obj = (JSONObject) itemJsonObj.get(Integer.toString(id));
                Items item = new Items(obj);
                System.out.println(item.toString());
                System.out.println("Enter the field to be updated");
                updateField = scan.readLine();
                if (updateField.equals("Item Name") || updateField.equals("Item Category")) {
                    System.out.println("Enter the value to be updated");
                    updateValue = scan.readLine();
                    obj.put(updateField, updateValue);
                } else {
                    System.out.println("Enter the value to be updated");
                    updateIntValue = Integer.parseInt(scan.readLine());
                    obj.put(updateField, updateIntValue);
                }
                System.out.println(itemJsonObj.toString());

            } else {
                System.out.println(invalidItemId);

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public JSONObject createNewCustomer(Customer object) throws IOException, ParseException {
        JSONObject newCustomer = null;
        int id = newCustomerId();
        String encryptPassword;

        Set<String> checkSet = (Set<String>) customerJsonObj.keySet();
        Set<String> names = new HashSet<>();

        for(String str :checkSet)
        {
            JSONObject checkObject = (JSONObject)customerJsonObj.get(str);
            names.add(checkObject.get("UserName").toString());

        }
        if(names.contains(object.getUserName()))
        {
            System.out.println("User Already exist . Try another user name");
        }
        else {
             newCustomer = new JSONObject();
            newCustomer.put("CustomerId", id);
            newCustomer.put("UserName", object.getUserName());
            encryptPassword = encrypt(object.getPassword());

            newCustomer.put("Password", encryptPassword);
            newCustomer.put("Role", object.getRole());
            newCustomer.put("CurrentStatus", object.getCurrentStatus());

            JSONParser parser = new JSONParser();
            JSONObject discount = (JSONObject) parser.parse(object.getDiscount());
            newCustomer.put("Discount", discount);
            customerJsonObj.put(Integer.toString(id), newCustomer);

            FileWriter write = new FileWriter("CustomerDetails.json");
            write.write(customerJsonObj.toString());
            write.close();
        }

        return newCustomer;
    }

    public JSONArray topSellingItems() {

        JSONArray items = new JSONArray();
        Set<String> keys = (Set<String>) purchaseJsonObj.keySet();
        HashMap<Integer, Integer> topSelling = new HashMap<>();

        for (String i : keys) {
            topSelling.put(Integer.parseInt(i), Integer.parseInt(purchaseJsonObj.get(i).toString()));
        }
        Map<Integer, Integer> hm = sortByValue(topSelling);
        int i = 1;
        for (Map.Entry<Integer, Integer> en : hm.entrySet()) {
            if (i < 4) {
                int key = en.getKey();
                JSONObject things = (JSONObject) itemJsonObj.get(Integer.toString(key));
                items.add(things);
                i++;
            }

        }
        return items;

    }

    public JSONObject loadFile(String file) {
        JSONParser parser = new JSONParser();
        JSONObject obj = null;
        try {


            File open = new File(file);
            FileReader read;
            if (open.length() != 0) {
                read = new FileReader(file);
                obj = (JSONObject) parser.parse(read);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return obj;
    }

    public JSONObject showAllItems() {
        return itemJsonObj;
    }

    public static int getOrderId() {
        int id = 1000;

        Set<String> order = (Set<String>) orderJsonObj.keySet();
        //TreeSet<Integer> orderId = (TreeSet<Integer>) orderJsonObj.keySet();
        TreeSet<String> orderId = new TreeSet<>(order);
        for (String i : orderId) {
            id = Integer.parseInt(i);
        }
        return ++id;
    }

    public JSONObject getCustomerDetails() {
        return customerJsonObj;
    }

    private void saveToFile() {
        try {
            FileWriter write1 = new FileWriter("CustomerDetails.json");
            FileWriter write2 = new FileWriter("ItemDetails.json");
            FileWriter write3 = new FileWriter("OrderDetails.json");
            FileWriter write4 = new FileWriter("OrderIdMapping.json");
            FileWriter write5 = new FileWriter("PurchaseList.json");
            write1.write(customerJsonObj.toString());
            write2.write(itemJsonObj.toString());
            write3.write(orderJsonObj.toString());
            write4.write(orderIdMapping.toString());
            write5.write(purchaseJsonObj.toString());
            write1.close();
            write2.close();
            write3.close();
            write4.close();
            write5.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    HashMap<Integer, Integer> sortByValue(HashMap<Integer, Integer> hm) {
        List<Map.Entry<Integer, Integer>> list = new LinkedList<>(hm.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            public int compare(Map.Entry<Integer, Integer> o1,
                               Map.Entry<Integer, Integer> o2) {
                if (o1.getValue() > o2.getValue()) {
                    return -1;
                } else if (o1.getValue() < o2.getValue()) {
                    return 1;
                } else
                    return 0;
            }
        });
        HashMap<Integer, Integer> temp = new LinkedHashMap<>();
        for (Map.Entry<Integer, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

    public JSONObject checkLogin() {
        JSONObject check = null;
        String decryptPass;
        Set<String> keys = (Set<String>) customerJsonObj.keySet();
        for (String str : keys) {
            JSONObject temp = (JSONObject) customerJsonObj.get(str);
            Customer customer = new Customer(temp);
            if (customer.getCurrentStatus().equals("active")) {
                check = new JSONObject();
                decryptPass = decrypt(customer.getPassword());
                check.put("id", customer.getCusId());
                check.put("password", decryptPass);
            }
        }

        return check;
    }

    public String decrypt(String s) {
        String password;
        char t = '0';
        int i = 0, m = 0;

        char[] b = s.toCharArray();

        while (i < b.length) {

            if (b[m] >= 'a' && b[m] <= 'z') {
                t = (char) (((b[m] - 'a') - 1) % 26);
            } else if (b[m] >= 'A' && b[m] <= 'Z') {
                t = (char) (((b[m] - 'A') - 1) % 26);
            } else if (Character.isDigit(b[m])) {
                int x = (int) b[m];
                // System.out.println(x);
                x = x - 1;
                //System.out.println(x);
                if (x < 48) {
                    t = '9';
                } else
                    t = (char) x;
            }


            if (b[m] >= 'a' && b[m] <= 'z')
                b[m] = (char) (t + 'a');
            else if (Character.isDigit(b[m])) {
                b[m] = t;
            } else
                b[m] = (char) (t + 'A');

            m++;
            i++;
        }

        password = new String(b);

        return password;
    }

    public String encrypt(String s) {
        String password;
        char t = '0';
        int i = 0, m = 0;

        char[] b = s.toCharArray();

        while (i < b.length) {

            if (b[m] >= 'a' && b[m] <= 'z') {
                t = (char) (((b[m] - 'a') + 1) % 26);
            } else if (b[m] >= 'A' && b[m] <= 'Z') {
                t = (char) (((b[m] - 'A') + 1) % 26);
            } else if (Character.isDigit(b[m])) {
                int x = (int) b[m];
                // System.out.println(x);
                x = x + 1;
                //System.out.println(x);
                if (x > 57) {
                    t = '0';
                } else
                    t = (char) x;
            }


            if (b[m] >= 'a' && b[m] <= 'z')
                b[m] = (char) (t + 'a');
            else if (Character.isDigit(b[m])) {
                b[m] = t;
            } else
                b[m] = (char) (t + 'A');

            m++;
            i++;
        }

        password = new String(b);

        return password;
    }

    public int newCustomerId() {
        int id = 99;

        Set<String> cus = (Set<String>) customerJsonObj.keySet();
        //TreeSet<Integer> orderId = (TreeSet<Integer>) orderJsonObj.keySet();
        TreeSet<String> orderId = new TreeSet<>(cus);
        for (String i : orderId) {
            id = Integer.parseInt(i);
        }
        return ++id;
    }

}


