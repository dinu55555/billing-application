package service;

import interfaces.Operations;

import model.Customer;
import model.Items;
import model.OrderDetails;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;


public class DBStorage implements Operations {

    private final String loginQuery = "select * from customer where id =?";
    private final String checkLogin = "select * from customer where status ='active';";
    private final String loginUpdate = "update customer set status =? where id=?";
    private final String insertOrderDetails = "insert into order_details values(?,?,?,?,?)";
    private final String orderIdSeq = "select nextval('order_details_seq')";
    private final String customerIdSeq = "select nextval('customer_seq')";
    private final String itemQuery = "select *  from items where id = ?";
    private final String updateCount = "update items set sold_count =? where id = ?";
    private final String updateAvailable = "update items set available_quantity =? where id = ?";
    private final String orderMapping = "insert into order_mapping values(?,?)";
    private final String deleteDiscount = "delete from discount_mapping where code=? and customer_id=?";
    private final String customerOrders = "select * from order_details where id in(select order_id from order_mapping where customer_id =?);";
    private final String logoutUpdate = "update customer set status =? where id=?";
    private final String topSelling = "select * from items order by sold_count desc limit 3;";
    private final String updateditem = "update item set ? = ? where id = ?;";
    private final String insertCustomer = "insert into customer values (?,?,?,?,?);";
    private final String dicountMap = "select code from discount;";
    private final String insertDiscount = "insert into discount_mapping values(?,?);";
    private final String getNames = "select name from customer;";





    LoadDataBase load = new LoadDataBase();
    static Connection c = null;

    public DBStorage(){
        c = load.getConnection(c);

    }

    public JSONObject placeOrder(JSONArray orders, int customerId, String discount) throws SQLException {
        JSONObject jsonObj;
        JSONObject object = new JSONObject();
        int id = 0, quantity, soldValue = 0, remainingValue = 0;
        double amountBeforeDiscount = 0, amountAfterDiscount = 0;
        for (Object obj : orders) {
            jsonObj = (JSONObject) obj;
            object.put(jsonObj.get("Item id"), jsonObj);
            //System.out.println(jsonObj.toString());
            OrderDetails order = new OrderDetails(jsonObj);
            quantity = order.getOrderedQuantity();
            amountBeforeDiscount += order.getTotalPrice();
            // System.out.println(order.toString());
//--------------------------------------------------------------------------
            PreparedStatement ps = c.prepareStatement(itemQuery);
            ps.setInt(1, order.getItemId());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                soldValue = rs.getInt("sold_count");
                remainingValue = rs.getInt("available_quantity");
            }
            ps = c.prepareStatement(updateCount);
            ps.setInt(1, quantity + soldValue);
            ps.setInt(2, order.getItemId());
            ps.executeUpdate();
            ps = c.prepareStatement(updateAvailable);
            ps.setInt(1, remainingValue - order.getOrderedQuantity());
            ps.setInt(2, order.getItemId());
            ps.executeUpdate();
        }
//--------------------------------------------------------------------------
        amountAfterDiscount = amountBeforeDiscount - (Double.parseDouble(discount) / 100) * amountBeforeDiscount;
        object.put("Amount Without Discount", amountBeforeDiscount);
        object.put("Discount", discount + "-percent");
        object.put("Final Amount with Discount", amountAfterDiscount);

        PreparedStatement ps = c.prepareStatement(deleteDiscount);
        ps.setInt(1, Integer.parseInt(discount));
        ps.setInt(2, customerId);
        ps.executeUpdate();

//---------------------------------------------------------------------------


        PreparedStatement ps1 = c.prepareStatement(orderIdSeq);
        ResultSet rs = ps1.executeQuery();
        if (rs.next()) {
            id = rs.getInt("nextval");
        }


        ps = c.prepareStatement(insertOrderDetails);
        ps.setInt(1, id);
        String jsonString = object.toString();
        ps.setString(2, jsonString);
        ps.setString(3, discount + "-percent");
        ps.setDouble(4, amountBeforeDiscount);
        ps.setDouble(5, amountAfterDiscount);
        ps.executeUpdate();
//---------------------------------------------------------------------------
        ps1 = c.prepareStatement(orderMapping);
        ps1.setInt(1, customerId);
        ps1.setInt(2, id);
        ps1.executeUpdate();
//---------------------------------------------------------------------------
        ps.close();
        rs.close();
        ps1.close();
        return object;
    }

    public JSONArray getOrderDetails(int customerId) throws SQLException, ParseException {
        JSONArray array = new JSONArray();
        PreparedStatement ps = c.prepareStatement(customerOrders);
        ps.setInt(1, customerId);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            String detail = rs.getString("details");
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(detail);
            json.put("Discount", rs.getString("discount_percent"));
            json.put("Amount Without Discount", rs.getDouble("amount_before_discount"));
            json.put("Final Amount with Discount", rs.getDouble("amount_after_discount"));
            array.add(json);

        }
        return array;

    }

    public String login(int id, String password) throws SQLException {
        String role = "null",decryptPass;

//-----------------------------------------------------------------

//-----------------------------------------------------------------

        PreparedStatement ps = c.prepareStatement(loginQuery);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            decryptPass=decrypt(rs.getString("password"));
            //System.out.println(decryptPass);
            if (decryptPass.equals(password)) {
                role = rs.getString("role");
                ps = c.prepareStatement(loginUpdate);
                ps.setString(1, "active");
                System.out.println("\t\tLogin Successful");
                System.out.println("Welcome "+rs.getString("name"));
                System.out.println();
                ps.setInt(2, id);
                ps.executeUpdate();
            } else
                System.out.println("Incorrect password . Retry");
        } else
            System.out.println("User Not Found . Retry");
        ps.close();
        rs.close();
        return role;

    }

    public boolean logout(int id) throws SQLException {

        PreparedStatement ps = c.prepareStatement(logoutUpdate);
        ps.setString(1, "logged out");
        ps.setInt(2, id);
        ps.executeUpdate();
        ps.close();
        System.out.println("Logged out Successfully");
        return true;

    }

    public JSONObject updateItem(int itemId) throws SQLException, IOException {
        JSONObject jsonObject = new JSONObject();
        String updateField, updateValue;
        int updateIntValue;
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        PreparedStatement ps = c.prepareStatement(itemQuery);
        ps.setInt(1, itemId);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            jsonObject.put("Item id", rs.getInt("id"));
            jsonObject.put("Item Name", rs.getString("name"));
            jsonObject.put("Item Category", rs.getString("category"));
            jsonObject.put("Price Per Item", rs.getDouble("price"));
            jsonObject.put("Available Quantity", rs.getInt("available_quantity"));
        }
        Items item = new Items(jsonObject);
        System.out.println(item.toString());
        System.out.println("Enter the field to be updated");
        updateField = scan.readLine();
        System.out.println("Enter the value to be updated");

        if(updateField.equals("Item Name") || updateField.equals("Item Category"))
        {
            updateValue = scan.readLine();
            jsonObject.put(updateField, updateValue);
            ps=c.prepareStatement(updateditem);
            ps.setString(1,updateField);
            ps.setString(2,updateValue);
        }
        else
        {
            updateIntValue = Integer.parseInt(scan.readLine());
            jsonObject.put(updateField, updateIntValue);
            ps.setString(1,updateField);
            ps.setInt(2,updateIntValue);
        }


        ps.close();
        rs.close();
        return jsonObject;
    }

    public JSONObject createNewCustomer(Customer object) throws ParseException, SQLException {
        JSONObject newCustomer = null;
        int id=0 ,flag=0;
        String encryptPassword;

        PreparedStatement ps1 = c.prepareStatement(getNames);
        ResultSet names = ps1.executeQuery();
        while(names.next())
        {
            if(names.getString("name").equals(object.getUserName()))
            {
                flag=1;
            }
        }
        if(flag == 1)
        {
            System.out.println("Username already exist . Try another Username");
        }
        else
        {
            newCustomer = new JSONObject();
            ps1 = c.prepareStatement(customerIdSeq);
            ResultSet rs = ps1.executeQuery();
            if (rs.next()) {
                id = rs.getInt("nextval");
            }
            newCustomer.put("CustomerId", id);
            newCustomer.put("UserName", object.getUserName());
            encryptPassword = encrypt(object.getPassword());

            newCustomer.put("Password", encryptPassword);
            newCustomer.put("Role", object.getRole());
            newCustomer.put("CurrentStatus", object.getCurrentStatus());

            JSONParser parser = new JSONParser();
            JSONObject discountJson = (JSONObject)parser.parse(object.getDiscount());
            newCustomer.put("Discount",discountJson);

            ps1=c.prepareStatement(insertCustomer);
            ps1.setInt(1,id);
            ps1.setString(2,object.getUserName());
            ps1.setString(3,object.getPassword());
            ps1.setString(4,object.getRole());
            ps1.setString(5,object.getCurrentStatus());
            ps1.executeUpdate();

            ps1=c.prepareStatement(dicountMap);
            ResultSet discount = ps1.executeQuery();
            while(discount.next())
            {
                ps1=c.prepareStatement(insertDiscount);
                ps1.setInt(1,id);
                ps1.setInt(2,discount.getInt("code"));
                ps1.executeUpdate();
            }
            discount.close();
        }
        ps1.close();
        return newCustomer;
    }
    public JSONArray topSellingItems() throws SQLException {
        JSONArray array = new JSONArray();
        PreparedStatement ps = c.prepareStatement(topSelling);
        ResultSet rs = ps.executeQuery();
        while(rs.next())
        {
            JSONObject top = new JSONObject();
            top.put("Item id", rs.getInt("id"));
            top.put("Item Name", rs.getString("name"));
            top.put("Item Category", rs.getString("category"));
            top.put("Price Per Item", rs.getDouble("price"));
            top.put("Available Quantity", rs.getInt("available_quantity"));
            //top.put("Sold Quantity",rs.getInt("sold_count"));
            array.add(top);
        }
        ps.close();
        rs.close();
        return array;
    }

    public JSONObject showAllItems() throws SQLException {

        JSONObject object = new JSONObject();
        String selectItems = "select * from items;";
        PreparedStatement ps = c.prepareStatement(selectItems);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Item id", Integer.toString(rs.getInt("id")));
            jsonObject.put("Item Name", rs.getString("name"));
            jsonObject.put("Price Per Item", rs.getInt("price"));
            jsonObject.put("Available Quantity", rs.getInt("available_quantity"));
            jsonObject.put("Item Category", rs.getString("category"));
            object.put(Integer.toString(rs.getInt("id")), jsonObject);

        }

        rs.close();
        ps.close();
        return object;

    }

    public JSONObject getCustomerDetails() throws SQLException {
        JSONObject jsonObject = new JSONObject();
        String customersQuery = "select * from customer";
        String discountQuery = "select * from discount where code in (select code from discount_mapping where customer_id = ?); ";
        PreparedStatement ps = c.prepareStatement(customersQuery);
        ResultSet rs = ps.executeQuery();
        JSONObject discount = new JSONObject();
        while (rs.next()) {
            JSONObject temp = new JSONObject();
            temp.put("Role", rs.getString("role"));
            temp.put("UserName", rs.getString("name"));
            temp.put("CurrentStatus", rs.getString("status"));
            temp.put("CustomerId", rs.getInt("id"));
            temp.put("Password", rs.getString("password"));
            PreparedStatement ps2 = c.prepareStatement(discountQuery);
            ps2.setInt(1, rs.getInt("id"));
            ResultSet rs1 = ps2.executeQuery();
            while (rs1.next()) {

                discount.put(Integer.toString(rs1.getInt("code")), rs1.getString("name"));
            }
            temp.put("Discount", discount);
            //System.out.println(temp.toString());
            jsonObject.put(Integer.toString(rs.getInt("id")), temp);
            rs1.close();
            ps2.close();
        }

        rs.close();
        ps.close();

        return jsonObject;
    }
    public JSONObject checkLogin() throws SQLException {

        String decryptPass;
        JSONObject loginJson = null ;
        PreparedStatement ps = c.prepareStatement(checkLogin);
        ResultSet rs = ps.executeQuery();
        while(rs.next())
        {
            loginJson = new JSONObject();
            //System.out.println(rs.getInt("id"));
            decryptPass=decrypt(rs.getString("password"));
            loginJson.put("id",rs.getInt("id"));
            loginJson.put("password",decryptPass);
        }
        ps.close();
        rs.close();
        return loginJson;
    }

    public String decrypt(String s)
    {
        String password;
        char t='0';
        int i=0,m=0;

        char[] b=s.toCharArray();

        while(i<b.length) {

            if(b[m]>='a' && b[m]<='z') {
                t = (char) (((b[m]-'a')-1)%26);
            }
            else if(b[m]>='A' && b[m]<='Z') {
                t = (char) (((b[m]-'A')-1)%26);
            }
            else if(Character.isDigit(b[m]))
            {
                int x = (int)b[m];
                // System.out.println(x);
                x=x-1;
                //System.out.println(x);
                if(x<48)
                {
                    t='9';
                }
                else
                    t=(char)x;
            }


            if(b[m]>='a'&& b[m]<='z')
                b[m] = (char)(t+'a');
            else if(Character.isDigit(b[m]))
            {
                b[m] = t;
            }
            else
                b[m] = (char)(t+'A');

            m++;
            i++;
        }

        password = new String(b);

        return password;
    }

    public String encrypt(String s) {
        String password;
        char t = '0';
        int i = 0, m = 0;

        char[] b = s.toCharArray();

        while (i < b.length) {

            if (b[m] >= 'a' && b[m] <= 'z') {
                t = (char) (((b[m] - 'a') + 1) % 26);
            } else if (b[m] >= 'A' && b[m] <= 'Z') {
                t = (char) (((b[m] - 'A') + 1) % 26);
            } else if (Character.isDigit(b[m])) {
                int x = (int) b[m];
                // System.out.println(x);
                x = x + 1;
                //System.out.println(x);
                if (x > 57) {
                    t = '0';
                } else
                    t = (char) x;
            }


            if (b[m] >= 'a' && b[m] <= 'z')
                b[m] = (char) (t + 'a');
            else if (Character.isDigit(b[m])) {
                b[m] = t;
            } else
                b[m] = (char) (t + 'A');

            m++;
            i++;
        }

        password = new String(b);

        return password;
    }
}
