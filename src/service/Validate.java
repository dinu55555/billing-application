package service;

import enums.Role;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Set;


public class Validate extends Sentences {

    BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

    public String validateUserName() throws IOException {
        String name;
        while (true) {
            System.out.println("Enter the UserName");
            name = scan.readLine();
            if (name.length() < 2) {
                System.out.println(invalidName);
            } else
                return name;

        }
    }

    public String validatePassword() throws IOException {
        String password;
        while (true) {
            System.out.println("Enter the Password");
            password = scan.readLine();
            if (password.length() < 5) {
                System.out.println(invalidPassword);
            } else
                return password;
        }
    }

    /*public String validateRole() throws IOException {

        String role;
        while (true) {
            try {
                System.out.println("Enter the Role");
                role = scan.readLine();
                Role.valueOf(role.toUpperCase());
                return role;
            } catch (IllegalArgumentException e) {
                System.out.println(e);
                System.out.println("Enter the Valid Role");
            }
        }


    }*/

    public String showDiscount(Map<String, String> map) {
        JSONObject obj = new JSONObject();
        Set<Map.Entry<String, String>> discount = map.entrySet();
        for (Map.Entry<String, String> temp : discount) {
            obj.put(temp.getKey(), temp.getValue());
        }
        return obj.toString();
    }
}
