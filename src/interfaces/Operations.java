package interfaces;


import model.Customer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.sql.SQLException;



public interface Operations {

    JSONObject placeOrder(JSONArray orders, int customerId, String discount) throws SQLException;

    JSONArray getOrderDetails(int customerId) throws SQLException, ParseException;

    String login(int id, String Password) throws SQLException;

    boolean logout(int id) throws IOException, SQLException;

    JSONObject updateItem(int itemId) throws SQLException, IOException;

    JSONArray topSellingItems() throws SQLException;

    JSONObject showAllItems() throws SQLException;

    JSONObject getCustomerDetails() throws SQLException;

    JSONObject checkLogin() throws SQLException;

    JSONObject createNewCustomer(Customer object) throws ParseException, SQLException, IOException;


}
