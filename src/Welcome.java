import interfaces.Operations;
import model.Customer;
import model.Items;
import model.OrderDetails;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import service.DBStorage;
import service.JsonStorage;
import service.Sentences;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Set;

public class Welcome extends Sentences {

    public static void main(String[] args) throws IOException, SQLException, ParseException {

        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));

        Operations type = new DBStorage();


        int customerId;
        String password, loginType;
        boolean exit = false;
        System.out.println("\t\tWELCOME TO ONLINE SHOPPING");
        while (true) {
        try
        {

                System.out.println("Enter\n1 to Login\n2 to Signup");

                int choose = Integer.parseInt(scan.readLine());
                if (choose == 2) {
                    Customer create = new Customer();
                    JSONObject newCustomerJson = type.createNewCustomer(create);
                    create = new Customer(newCustomerJson);
                    System.out.println(create.toString());
                } else
                    break;
            }
        catch(NumberFormatException e)
        {
            System.out.println("Invalid input");
        }
        catch(NullPointerException e)
        {
            System.out.println("Invalid \n");
        }
        }


        do {
            int choice;
            JSONObject loginJson;
            loginJson = type.checkLogin();
            if (loginJson == null) {
                while (true) {
                    System.out.println("\nLogin");
                    System.out.println("-----");
                    System.out.println("Enter the Customer ID");
                    try {
                        customerId = Integer.parseInt(scan.readLine());
                        break;
                    } catch (NumberFormatException e) {
                        System.out.println("Please Enter some Id");
                        System.out.println();
                    }
                }


                System.out.println("Enter the Password");
                password = scan.readLine();
            } else {
                customerId = Integer.parseInt(loginJson.get("id").toString());
                password = loginJson.get("password").toString();
            }

            loginType = type.login(customerId, password);
            if (loginType.equals(admin)) {
                System.out.println("You have logged in  as Admin");
                do {
                    System.out.println("Enter");
                    System.out.println("1 to Update Item");
                    System.out.println("2 to find the top selling items");
                    System.out.println("3 to logout");
                    while (true) {
                        try {
                            choice = Integer.parseInt(scan.readLine());
                            break;
                        } catch (NumberFormatException e) {
                            System.out.println("Enter any option");
                            System.out.println();
                        } catch (NullPointerException e) {
                            System.out.println("No Values found");
                            System.out.println();
                        }
                    }


                    switch (choice) {
                        case 1: {
                            try {
                                JSONObject obj;
                                int id;
                                System.out.println("Enter the Item Id to update it");
                                id = Integer.parseInt(scan.readLine());
                                obj = type.updateItem(id);
                                Items item = new Items(obj);
                                System.out.println(item.toString());
                                //System.out.println(obj.toString());
                                break;
                            } catch (NumberFormatException e) {
                                System.out.println("Invalid Action");
                                System.out.println();
                                break;
                            } catch (NullPointerException e) {
                                System.out.println("No Values found");
                                System.out.println();
                                break;
                            }

                        }

                        case 2: {
                            try {
                                JSONArray purchaseList;
                                purchaseList = type.topSellingItems();
                                for (Object obj : purchaseList) {
                                    JSONObject item = (JSONObject) obj;
                                    //System.out.println(item.toString());
                                    Items names = new Items(item);
                                    System.out.println(names.toString());

                                }
                                break;
                            } catch (NullPointerException e) {
                                System.out.println("No Value found");
                                System.out.println();
                                break;
                            }

                        }

                        case 3: {

                            type.logout(customerId);
                            break;
                        }
                        default: {
                            System.out.println("Enter the correct option");
                            System.out.println();
                        }
                    }

                } while (choice != 3);


            } else if (loginType.equals(customer)) {
                do {
                    System.out.println("Enter");
                    while (true) {
                        System.out.println("1 to Place an Order");
                        System.out.println("2 to View the order History");
                        System.out.println("3 to logout");

                        try {
                            choice = Integer.parseInt(scan.readLine());
                            break;
                        } catch (NumberFormatException e) {
                            System.out.println("Enter any option");
                            System.out.println();
                        }
                    }

                    switch (choice) {
                        case 1: {

                            try {
                                JSONObject obj = type.showAllItems();
                                Set<String> keyString = (Set<String>) obj.keySet();
                                for (String str : keyString) {
                                    JSONObject temp = (JSONObject) obj.get(str);
                                    Items show = new Items(temp);
                                    System.out.println(show);
                                    System.out.println();
                                }
                                //System.out.println("Stock - " + obj.toString());
                                JSONObject orderedObject;
                                JSONArray orders = new JSONArray();
                                int itemId, quantity, flag, choose;
                                String discountCode = "0";
                                boolean status = true;
                                String say;
                                //System.out.println(obj);
                                do {
                                    System.out.println("Enter the Item Id ");
                                    itemId = Integer.parseInt(scan.readLine());
                                    orderedObject = (JSONObject) obj.get(Integer.toString(itemId));
                                    //System.out.println(orderedObject.toString());
                                    Items item = new Items(orderedObject);
                                    System.out.println(item.toString());
                                    System.out.println();
                                    System.out.println("Enter the Quantity ");
                                    quantity = Integer.parseInt(scan.readLine());
                                    if (quantity <= Integer.parseInt(orderedObject.get(availableQuantity).toString())) {
                                        flag = 1;
                                        OrderDetails order = new OrderDetails(orderedObject, quantity);
                                        orders.add(order.jsonObject);
                                        System.out.println("Continue to buy another product . Enter 1 to continue 0 to end");
                                        choose = Integer.parseInt(scan.readLine());
                                    } else {
                                        flag = 0;
                                        System.out.println("Insufficient Stock . Enter the correct amount");
                                        choose = 1;
                                    }

                                } while (choose == 1);

                                if (flag == 1)

                                {
                                    JSONObject temporary = type.getCustomerDetails();
                                    //System.out.println(temporary);
                                    JSONObject activeCus = (JSONObject) temporary.get(Integer.toString(customerId));
                                    //System.out.println(activeCus);
                                    System.out.println("Do you wish to use the Discount Coupon . Enter 'yes' to use or 'no' to ignore ");
                                    say = scan.readLine();
                                    if (say.toLowerCase().equals("yes")) {
                                        try {
                                            JSONObject discount = (JSONObject) activeCus.get("Discount");
                                            Set<String> dicountSet = (Set<String>) discount.keySet();
                                            System.out.println("Code" + " \t-  Name");
                                            for (String key : dicountSet) {
                                                System.out.println(" " + key + " \t-  " + discount.get(key));
                                            }
                                            do {
                                                System.out.println("Enter the required coupon code");
                                                discountCode = scan.readLine();
                                                if (dicountSet.contains(discountCode)) {
                                                    status = false;
                                                } else {
                                                    System.out.println("Invalid Code . Enter the correct coupon");
                                                }
                                            } while (status);
                                        } catch (NullPointerException e) {
                                            System.out.println("No Discounts Available\n");
                                        }
                                    }
                                    JSONObject orderPlaced = type.placeOrder(orders, customerId, discountCode);
                                    System.out.println("\t\tOrder Details");
                                    Set<String> keys = (Set<String>) orderedObject.keySet();
                                    for (String str : keys) {
                                        if (str.length() < 2 && (Integer.parseInt(str) == 1 || Integer.parseInt(str) == 2 || Integer.parseInt(str) == 3 || Integer.parseInt(str) == 4)) {
                                            JSONObject obj2 = (JSONObject) orderedObject.get(str);
                                            OrderDetails order = new OrderDetails(obj2);
                                            System.out.println(order.toString());
                                            System.out.println();
                                        }

                                    }
                                    System.out.println("Discount - " + orderPlaced.get("Discount"));
                                    System.out.println("Amount Without Discount - " + orderPlaced.get("Amount Without Discount"));
                                    System.out.println("Final Amount with Discount - " + orderPlaced.get("Final Amount with Discount"));
                                    System.out.println("--------------------------------------------------");
                                }
                                break;
                            } catch (NumberFormatException e) {
                                System.out.println("Invalid Entry");
                                System.out.println();
                                break;
                            } catch (NullPointerException e) {
                                System.out.println("No Values found");
                                System.out.println();
                                break;
                            }
                        }

                        case 2: {

                            try {
                                JSONArray orderDetails = type.getOrderDetails(customerId);
                                for (Object object : orderDetails) {
                                    JSONObject obj = (JSONObject) object;
                                    //System.out.println(obj.toString());
                                    Set<String> keys = (Set<String>) obj.keySet();
                                    for (String str : keys) {
                                        if (str.length() < 2 && (Integer.parseInt(str) == 1 || Integer.parseInt(str) == 2 || Integer.parseInt(str) == 3 || Integer.parseInt(str) == 4)) {
                                            JSONObject obj2 = (JSONObject) obj.get(str);
                                            OrderDetails order = new OrderDetails(obj2);
                                            System.out.println(order.toString());
                                            System.out.println();
                                        }

                                    }
                                    System.out.println("Discount - " + obj.get("Discount"));
                                    System.out.println("Amount Without Discount - " + obj.get("Amount Without Discount"));
                                    System.out.println("Final Amount with Discount - " + obj.get("Final Amount with Discount"));
                                    System.out.println("--------------------------------------------------");

                                }
                                break;
                            } catch (NullPointerException e) {
                                System.out.println("No values found");
                                System.out.println();
                                break;
                            }

                        }

                        case 3: {

                            type.logout(customerId);
                            break;
                        }
                        default: {
                            System.out.println("Enter the correct option");
                            System.out.println();
                        }
                    }

                } while (choice != 3);
            } else {
                exit = true;
            }
        } while (exit);

    }
}
